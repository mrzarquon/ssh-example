#!/bin/bash

# only do this in the presence of the SSH_PRIV_KEY set
# over here: https://gitlab.com/mrzarquon/ssh-example
if [[ -v SSH_PRIV_KEY && ! -z "$SSH_PRIV_KEY" ]]; then
    echo "SSH_PRIV_KEY is set"
    mkdir -p /home/gitpod/.ssh
    chmod 700 /home/gitpod/.ssh
    echo "${SSH_PRIV_KEY}" | base64 -d > /home/gitpod/.ssh/env_private_key
    chmod 600 /home/gitpod/.ssh/env_private_key
    cat <<- SSHCONFIG > /home/gitpod/.ssh/config
Host *
    AddKeysToAgent yes
    IdentityFile /home/gitpod/.ssh/env_private_key
SSHCONFIG
fi