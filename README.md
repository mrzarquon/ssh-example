# Ssh Example

## SSH with [Tailscale](https://tailscale.com/kb/1193/tailscale-ssh/)

The easiest and most secure option because it uses Tailscale's network and trust to perform the SSH session and doesn't require SSH private keys at all. It does require the workspace and the target to have tailscale installed, but once it is installed, it is as easy as `ssh username@targets-tailscale-name` or `tailscale ssh username@targets-tailscale-name` if magicDNS is not enabled.

## SSH Agent Forwarding

This requires an additional step to configure a users desktop to forward their private key to gitpod when they connect over SSH. This also means that you have to use the same private key you connect to Gitpod with that you then connect to other services with.

The benefit of this is you are only storing your SSH Public Key in Gitpod's servers, instead of sensitive key material.

On the desktop you are forwarding the SSH key from, you need to update your ssh config file:
```
Host *.ssh.ws*.gitpod.io
    ForwardAgent yes
    IdentityFile /Users/cbarker/.ssh/gitpod 
    # other beneficial tunings
    ServerAliveInterval 60
    ServerAliveCountMax 10
```

Then add the public key corresponding to the private key used for `IdentityFile` to your Gitpod [SSH settings](https://www.gitpod.io/docs/configure/user-settings/ssh)

Opening a workspace in a desk IDE will automatically use the SSH session and forward your private key, but if you are in a browser editor, you can still ssh using the `Connect via SSH` menu option in the workspaces page.

The ssh connection string based on your workspace ID and host, environment variables in your workspace: so you can get the connection string from a workspace terminal with:
```
echo "ssh $GITPOD_WORKSPACE_ID@$GITPOD_WORKSPACE_ID.$GITPOD_WORKSPACE_CLUSTER_HOST"
```

## SSH with privkey stored in environment variable

Storing a private key as a ENV variable can have complications (it is storing sensitive material in Gitpods servers), but does allow you to automatically get a key. Requiring a password for the key is essential to keep it secure even if the ENV is leaked.

Convert a key to base64
```
cat ~/.ssh/my_private_key_for_gitpod_use | base64
```

Store this as a [user environment variable](https://www.gitpod.io/docs/configure/projects/environment-variables#user-specific-environment-variables) with the name SSH_PRIV_KEY, scoped only for the repo you want to use it in.

If this for a personal user's preference, then deploying / installing the ssh key would be done via a [dotfiles](https://www.gitpod.io/docs/configure/user-settings/dotfiles) script
```bash
if [[ -v SSH_PRIV_KEY && ! -z "$SSH_PRIV_KEY" ]]; then
    echo "SSH_PRIV_KEY is set"
    mkdir -p /home/gitpod/.ssh
    chmod 700 /home/gitpod/.ssh
    echo "${SSH_PRIV_KEY}" | base64 -d > /home/gitpod/.ssh/env_private_key
    chmod 600 /home/gitpod/.ssh/env_private_key
    cat <<- SSHCONFIG > /home/gitpod/.ssh/config
Host *
    AddKeysToAgent yes
    IdentityFile /home/gitpod/.ssh/env_private_key
SSHCONFIG
fi
```

If everyone accessing a particular repo needs to use their own SSH in the workspace, they could use a task like in this repo that installs a private key for them at launch.
```yaml
tasks:
  - name: create-ssh-key
    command: |
        mkdir -p ~/.ssh
        chmod 700 ~/.ssh
        echo "${SSH_PRIV_KEY}" | base64 -d > /home/gitpod/.ssh/env_private_key
        chmod 600 /home/gitpod/.ssh/env_private_key
```
